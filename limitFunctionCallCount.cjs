function limitFunctionCallCount(cb, n) {
  let count = n;
  return function (...args) {
    if (count == 0) {
      return null;
    } else {
      count--;
      return cb(...args);
    }
  };
}
module.exports = limitFunctionCallCount;