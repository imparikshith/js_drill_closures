function counterFactory() {
  let count = 0;
  return {
    increment: function increment() {
      return ++count;
    },
    decrement: function decrement() {
      return --count;
    },
  };
}
module.exports = counterFactory;