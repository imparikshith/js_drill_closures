function cacheFunction(cb) {
  let cacheObject = [];
  var flag = true;
  return function (...args) {
    const key = JSON.stringify(args);
    if (!JSON.stringify(cacheObject).includes(key) && flag) {
      cacheObject.push([...args]);
      return cb(...args);
    } else {
      flag = false;
      console.log(cacheObject);
      return null;
    }
  };
}
module.exports = cacheFunction;