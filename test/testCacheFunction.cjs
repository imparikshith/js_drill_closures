const cacheFunction = require("../cacheFunction.cjs");
const multiply = cacheFunction((x, y) =>
  console.log(`Product of ${x} and ${y} is ${x * y}.`)
);
multiply(3, 2);
multiply(4, 5);
multiply(2, 3);
multiply(4, 5);