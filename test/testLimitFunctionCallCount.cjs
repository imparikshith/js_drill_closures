const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");
const addition = limitFunctionCallCount(
  (x, y) => console.log(`Sum of ${x} and ${y} is ${x + y}.`),
  3
);
addition(4, 9);
addition(1, 5);
addition(-1, 6);
addition(8, 7);